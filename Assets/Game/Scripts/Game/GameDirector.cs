﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameDirector : Singleton<GameDirector>
{
	public int ScoreValue = 0;
	public Text ScoreText;
	public Text endGameText;
	public bool gameOver = false;
	public float PowerPelletTime = 2.0f;
	private float powerPelletCounter = 0.0f;
	public List<GhostController> Ghosts = new List<GhostController>();
	private float timer;
	public float scatterTime = 7.0f;
	public float chaseTime = 20.0f;

	public static int BlinkyIndex = 0;

	public enum States
	{
		enState_Normal,
		enState_PacmanInvincible,
		enState_GameOver,
	};

	public enum GhostStates
	{
		Chase,
		Scatter,
		Frightened
	};

	public States state = States.enState_Normal;
	public GhostStates ghostState = GhostStates.Scatter;
	public GhostStates previousGhostState = GhostStates.Scatter;

	[System.Serializable]
	public class GameStateChangedEvent : UnityEvent<GameDirector.States> { }
	public GameStateChangedEvent GameStateChanged;

	[System.Serializable]
	public class GhostStateChangedEvent : UnityEvent<GameDirector.GhostStates> { }
	public GhostStateChangedEvent GhostStateChanged;

	void Start()
	{
		timer = 0;
		string formatString = System.String.Format("{0:D9}", ScoreValue);
		ScoreText.text = formatString;
	}

	public void IncreaseScore(int value)
	{
		ScoreValue += value;
		string formatString = System.String.Format("{0:D9}", ScoreValue);
		ScoreText.text = formatString;
	}

	public void ChangeGameState(States _newState)
	{
		state = _newState;
		switch (state)
		{
			case States.enState_Normal:
				powerPelletCounter = 0.0f;
				ChangeGhostState(previousGhostState);
				break;

			case States.enState_PacmanInvincible:
				powerPelletCounter = 0.0f;
				// Can chain frightened states, so ignore 
				// any new frightened states while frightened
				if (ghostState != GhostStates.Frightened) {
					previousGhostState = ghostState;
				}
				ChangeGhostState(GhostStates.Frightened);
				break;

			case States.enState_GameOver:
				gameOver = true;
				endGameText.gameObject.SetActive(true);
				endGameText.text = "Game Over!";
				StartCoroutine(ReloadGame());
				break;
		}
		GameStateChanged.Invoke(state);
	}

	public void ChangeGhostState(GhostStates newState) {
		ghostState = newState;
		GhostStateChanged.Invoke(ghostState);
    }

	void UpdateGhostTimer() {
		// In frightened state, the timer doesn't update
		// When frightened ends, the ghost goes back to its previous state
		// and the timer resumes where it left off
		if (ghostState == GhostStates.Frightened) {
			return;
        }

		timer += Time.deltaTime;
		if (ghostState == GhostStates.Chase) {
			if (timer >= chaseTime) {
				timer = 0;
				ChangeGhostState(GhostStates.Scatter);
            }
        } else if (ghostState == GhostStates.Scatter){
			if (timer >= scatterTime) {
				timer = 0;
				ChangeGhostState(GhostStates.Chase);
            }
        }
    }

	public void Update()
	{
		switch(state)
		{
			case States.enState_PacmanInvincible:
				powerPelletCounter += Time.deltaTime;
				if (powerPelletCounter >= PowerPelletTime)
				{
					ChangeGameState(States.enState_Normal);
				}
				break;
		}

		UpdateGhostTimer();
	}

	IEnumerator ReloadGame() {
		yield return new WaitForSeconds(2f);
		SceneManager.LoadScene(0);
    }
}
