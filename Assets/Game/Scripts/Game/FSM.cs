using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FSM : MonoBehaviour
{
    public RuntimeAnimatorController FSMController;
    protected GameObject FSMGO;

    void Awake() {
        // Create an FSM GameObject and set this as a child to the gameObject
        FSMGO = new GameObject("FSM", typeof(Animator));
        FSMGO.transform.parent = transform;

        // Get the animator from the FSM
        Animator animator = FSMGO.GetComponent<Animator>();
        animator.runtimeAnimatorController = FSMController;

        // Hide the animator (optional)
        animator.hideFlags = HideFlags.HideInInspector;

        // Iterate over all states in the controller and set the owner
        FSMBaseState[] behaviours = animator.GetBehaviours<FSMBaseState>();
        foreach(var behaviour in behaviours) {
            behaviour.Init(gameObject, this);
        }
    }
}
