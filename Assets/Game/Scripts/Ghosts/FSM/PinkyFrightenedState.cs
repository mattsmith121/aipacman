using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkyFrightenedState : GhostBaseState
{
    public Vector2[] hidingLocations;
    public float timeThreshold = 1f;
    private float timer;
    private int index;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        timer = 0;
        index = 0;
    }

    // Pinky will cycle between locations when frightened
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        // if locations not set, default to spawn
        if (hidingLocations.Length <= 0) {
            ghost.SetTarget(new Vector2(0, 2));
        }

        timer += Time.deltaTime;
        if (timer >= timeThreshold) {
            timer = 0;
            index++;
            if (index >= hidingLocations.Length) {
                index = 0;
            }
        }

        ghost.SetTarget(hidingLocations[index]);
    }
}
