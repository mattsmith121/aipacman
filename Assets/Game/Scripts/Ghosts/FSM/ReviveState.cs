using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReviveState : GhostBaseState
{
    public Vector2 reviveTile = new Vector2(0, -1);
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        ghost.SetTarget(reviveTile);
    }

    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if ((int)ghost.transform.position.x == reviveTile.x &&
            (int)ghost.transform.position.y == reviveTile.y) {
            ghost.ReviveGhost();
        }
    }
}
