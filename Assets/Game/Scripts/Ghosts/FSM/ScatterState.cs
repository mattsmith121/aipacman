using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScatterState : GhostBaseState
{
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        ghost.SetTarget(ghost.HomeTile);
    }
}
