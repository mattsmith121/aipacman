using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClydeChaseState : GhostBaseState
{
    public int chaseDistance = 8;
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        Vector3 curPos = ghost.transform.position;
        float distance = Vector3.Distance(ghost.PacMan.position, curPos);
        if (distance >= chaseDistance) {
            ghost.SetTarget(ghost.PacMan.position);
        }
        else {
            ghost.SetTarget(ghost.HomeTile);
        }
    }
}
