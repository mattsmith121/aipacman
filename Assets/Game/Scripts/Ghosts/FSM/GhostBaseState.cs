using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class GhostBaseState : FSMBaseState
{
    protected GhostController ghost;

    public override void Init(GameObject _owner, FSM _fsm) {
        base.Init(_owner, _fsm);

        ghost = _owner.GetComponent<GhostController>();
    }
}
