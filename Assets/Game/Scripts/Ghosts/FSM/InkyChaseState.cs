using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InkyChaseState : GhostBaseState
{
    public float tilesInFront = 2f;
    public float vectorDistance = 2f;
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        // Get tilesInFront tiles in front of where Pacman is facing
        // Get Pacman Direction
        PacmanController.MoveDirection pacDir = ghost.PacMan.GetComponent<PacmanController>().moveDirection;
        Vector3 facing;
        if (pacDir == PacmanController.MoveDirection.Left) {
            facing = Vector3.left;
        } else if (pacDir == PacmanController.MoveDirection.Right) {
            facing = Vector3.right;
        } else if (pacDir == PacmanController.MoveDirection.Down) {
            facing = Vector3.down;
        } else { // up
            facing = Vector3.up;
        }
        // Get Pacman position
        Vector3 pacPos = ghost.PacMan.position;
        // Get x tiles in front of Pacman
        Vector3 tt = pacPos + facing * tilesInFront;

        // Get Vector from Blinky to in front of Pacman
        Vector3 blinkyPos = GameDirector.Instance.Ghosts[GameDirector.BlinkyIndex].transform.position;
        Vector3 bTp = tt - blinkyPos;
        // Multiply the Vector from blinky position to get new tile
        Vector3 target = blinkyPos + bTp * vectorDistance;
        ghost.SetTarget(target);
    }
}
