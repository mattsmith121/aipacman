using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinkyChaseState : GhostBaseState
{
    public float tilesInFront = 2f;
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        // Get tilesInFront tiles in front of where Pacman is facing
        // Get Pacman Direction
        PacmanController.MoveDirection pacDir = ghost.PacMan.GetComponent<PacmanController>().moveDirection;
        Vector3 facing;
        if (pacDir == PacmanController.MoveDirection.Left) {
            facing = Vector3.left;
        }
        else if (pacDir == PacmanController.MoveDirection.Right) {
            facing = Vector3.right;
        }
        else if (pacDir == PacmanController.MoveDirection.Down) {
            facing = Vector3.down;
        }
        else { // up
            facing = Vector3.up;
        }
        // Get Pacman position
        Vector3 pacPos = ghost.PacMan.position;
        // Get tilesInFront tiles in front of Pacman
        Vector3 tt = pacPos + facing * tilesInFront;

        // Set target
        ghost.SetTarget(tt);
    }
}
