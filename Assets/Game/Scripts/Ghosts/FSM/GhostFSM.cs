using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GhostController))]
public class GhostFSM : FSM
{
    GameDirector.GhostStates curState;
    GameDirector.GhostStates preFrightState;
    void Start() {
        // Ghosts start in scatter state
        curState = GameDirector.GhostStates.Scatter;
        GameDirector.Instance.GhostStateChanged.AddListener(GhostStateChanged);
    }

    public void GhostStateChanged(GameDirector.GhostStates newState) {
        // Don't want to transition to a state we are already in
        // Trigger won't be consumed
        if (newState == curState) {
            return;
        }

        if (newState == GameDirector.GhostStates.Frightened) {
            FSMGO.GetComponent<Animator>().SetTrigger("Frightened");
            preFrightState = curState;
        }
        else if (newState == GameDirector.GhostStates.Chase) {
            FSMGO.GetComponent<Animator>().SetTrigger("Chase");
        }
        else { // Scatter
            FSMGO.GetComponent<Animator>().SetTrigger("Scattered");
        }
        // Update current state
        curState = newState;
    }

    public void KillGhost() {
        FSMGO.GetComponent<Animator>().SetTrigger("Dead");
    }

    public void ReviveGhost() {
        if (preFrightState == GameDirector.GhostStates.Chase) {
            FSMGO.GetComponent<Animator>().ResetTrigger("Chase");
            FSMGO.GetComponent<Animator>().SetTrigger("rChase");
        } else {
            FSMGO.GetComponent<Animator>().ResetTrigger("Scattered");
            FSMGO.GetComponent<Animator>().SetTrigger("rScattered");
        }

        // Update current state to the state we were before frightened
        curState = preFrightState;
    }
}
