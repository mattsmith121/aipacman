using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkyFrightenedState : GhostBaseState
{
    // Blinky will try to go home when he is scared
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        ghost.SetTarget(ghost.HomeTile);
    }
}
