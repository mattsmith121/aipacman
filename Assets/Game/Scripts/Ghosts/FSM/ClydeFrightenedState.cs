using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClydeFrightenedState : GhostBaseState
{
    public Vector2 spawnLocation = new Vector2(0, 2);

    // Clyde will try to return to spawn
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        ghost.SetTarget(spawnLocation);
    }
}
