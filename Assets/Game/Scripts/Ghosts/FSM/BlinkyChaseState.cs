using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlinkyChaseState : GhostBaseState
{
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        ghost.SetTarget(ghost.PacMan.position);
    }
}
