﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AStarPathfinding;
using UnityEngine.Events;
using DG.Tweening;
using UnityEngine.Tilemaps;

public class GhostController : MonoBehaviour
{
	public enum Direction
    {
		up,
		down,
		left,
		right
    };
	Direction dir;
    public Direction startDirection;

	public Tilemap walls;
	public Tilemap intersections;

	public Vector2 HomeTile = new Vector2(0, 0);

	private Animator _animator;
	public Transform PacMan;
	public float speed;

	private Vector2 target;
    private bool dead;

    public GameDirector.GhostStates startState = GameDirector.GhostStates.Scatter;

	void Start()
	{
		dir = startDirection;
		_animator = GetComponent<Animator>();
		GameDirector.Instance.GameStateChanged.AddListener(GameStateChanged);
		// Start moving
		PathCompletedHandler();

        GetComponent<GhostFSM>().GhostStateChanged(startState);
	}

	public void Update()
	{
		if (GameDirector.Instance.gameOver == true)
		{
			return;
		}
	}

	public void onMoveCompleted()
	{
		PathCompletedHandler();
	}

	public void Kill()
	{
		_animator.SetBool("IsDead", true);
		GetComponent<GhostFSM>().KillGhost();
	}

	public void ReviveGhost() {
		_animator.SetBool("IsDead", false);
		_animator.SetBool("IsGhost", false);
		GetComponent<GhostFSM>().ReviveGhost();
	}

	public void SetTarget(Vector2 newTarget) {
		target = newTarget;
    }

	public void GameStateChanged(GameDirector.States _state)
	{
		switch (_state)
		{
			case GameDirector.States.enState_Normal:
				_animator.SetBool("IsGhost", false);
				break;

			case GameDirector.States.enState_PacmanInvincible:
				_animator.SetBool("IsGhost", true);
				break;

			case GameDirector.States.enState_GameOver:
                dead = true;
				break;
		}
	}

	// When we reach a tile, we already know what direction we are moving next
	// We need to decide what direction we are going to move AFTER that
	// **NOTE**: It was accepted that I didn't have to use the Pathfinder for ghost movement
	public void PathCompletedHandler() {
        if (dead) {
            return;
        }
        // Note priority is up > left > down
        // Get the tile we will be next
        #region Left
        if (dir == Direction.left) {
            Vector3Int pos = GetMapCoord(transform.position + Vector3.left);
            // Check if we are in front of an intersection
            if (intersections.GetTile(pos)) {
                // Make sure we can't leave play area
                bool noLeft = CheckNoLeft(intersections.GetTile(pos));
                bool noRight = CheckNoRight(intersections.GetTile(pos));
                // Check Up, left, down
                float uD, lD, dD;
                if (!walls.GetTile(pos + Vector3Int.up)) {
                    uD = Vector2.Distance(pos + Vector3.up, target);
                }
                else {
                    uD = 9999f;
                }

                if (!noLeft && !walls.GetTile(pos + Vector3Int.left)) {
                    lD = Vector2.Distance(pos + Vector3.left, target);
                }
                else {
                    lD = 9999f;
                }

                if (!walls.GetTile(pos + Vector3Int.down)) {
                    dD = Vector2.Distance(pos + Vector3.down, target);
                }
                else {
                    dD = 9999f;
                }

                if (uD <= lD && uD <= dD) {
                    dir = Direction.up;
                }
                else if (lD <= uD && lD <= dD) {
                    dir = Direction.left;
                }
                else { // go down
                    dir = Direction.down;
                }

            }
            // Check if we are going to be in front of a wall
            else if (walls.GetTile(pos + Vector3Int.left)) {
                // Go either down or up, can't go right
                if (!walls.GetTile(pos + Vector3Int.up)) {
                    dir = Direction.up;
                }
                else if (!walls.GetTile(pos + Vector3Int.down)) {
                    dir = Direction.down;
                }
            }
            // No matter what, we are moving left one tile next
            transform.DOMove(transform.position + Vector3.left, speed).OnComplete(onMoveCompleted).SetEase(Ease.Linear);
        }
        #endregion
        #region Right
        else if (dir == Direction.right) {
            Vector3Int pos = GetMapCoord(transform.position + Vector3.right);
            // Check if we are in front of an intersection
            if (intersections.GetTile(pos)) {
                // Make sure we can't leave play area
                bool noLeft = CheckNoLeft(intersections.GetTile(pos));
                bool noRight = CheckNoRight(intersections.GetTile(pos));
                // Check Up, down, right
                float uD, rD, dD;
                if (!walls.GetTile(pos + Vector3Int.up)) {
                    uD = Vector2.Distance(pos + Vector3.up, target);
                }
                else {
                    uD = 9999f;
                }

                if (!noRight && !walls.GetTile(pos + Vector3Int.right)) {
                    rD = Vector2.Distance(pos + Vector3.right, target);
                }
                else {
                    rD = 9999f;
                }

                if (!walls.GetTile(pos + Vector3Int.down)) {
                    dD = Vector2.Distance(pos + Vector3.down, target);
                }
                else {
                    dD = 9999f;
                }

                if (uD <= rD && uD <= dD) {
                    dir = Direction.up;
                }
                else if (dD <= uD && dD <= rD) {
                    dir = Direction.down;
                }
                else { // go right
                    dir = Direction.right;
                }

            }
            // Check if we are going to be in front of a wall
            else if (walls.GetTile(pos + Vector3Int.right)) {
                // Go either down or up, can't go left
                if (!walls.GetTile(pos + Vector3Int.up)) {
                    dir = Direction.up;
                }
                else if (!walls.GetTile(pos + Vector3Int.down)) {
                    dir = Direction.down;
                }
            }
            // No matter what, we are moving right one tile next
            transform.DOMove(transform.position + Vector3.right, speed).OnComplete(onMoveCompleted).SetEase(Ease.Linear);
        }
        #endregion
        #region Up
        else if (dir == Direction.up) {
            Vector3Int pos = GetMapCoord(transform.position + Vector3.up);
            // Check if we are in front of an intersection
            if (intersections.GetTile(pos)) {
                // Make sure we can't leave play area
                bool noLeft = CheckNoLeft(intersections.GetTile(pos));
                bool noRight = CheckNoRight(intersections.GetTile(pos));
                // Check Up, left, right
                float uD, rD, lD;
                if (!walls.GetTile(pos + Vector3Int.up)) {
                    uD = Vector2.Distance(pos + Vector3.up, target);
                }
                else {
                    uD = 9999f;
                }

                if (!noRight && !walls.GetTile(pos + Vector3Int.right)) {
                    rD = Vector2.Distance(pos + Vector3.right, target);
                }
                else {
                    rD = 9999f;
                }

                if (!noLeft && !walls.GetTile(pos + Vector3Int.left)) {
                    lD = Vector2.Distance(pos + Vector3.left, target);
                }
                else {
                    lD = 9999f;
                }

                if (uD <= rD && uD <= lD) {
                    dir = Direction.up;
                }
                else if (lD <= uD && lD <= rD) {
                    dir = Direction.left;
                }
                else { // go right
                    dir = Direction.right;
                }

            }
            // Check if we are going to be in front of a wall
            else if (walls.GetTile(pos + Vector3Int.up)) {
                // Go either left or right, can't go down
                if (!walls.GetTile(pos + Vector3Int.left)) {
                    dir = Direction.left;
                }
                else if (!walls.GetTile(pos + Vector3Int.right)) {
                    dir = Direction.right;
                }
            }
            // No matter what, we are moving up one tile next
            transform.DOMove(transform.position + Vector3.up, speed).OnComplete(onMoveCompleted).SetEase(Ease.Linear);
        }
        #endregion
        #region Down
        else { // down
            Vector3Int pos = GetMapCoord(transform.position + Vector3.down);
            // Check if we are in front of an intersection
            if (intersections.GetTile(pos)) {
                // Make sure we can't leave play area
                bool noLeft = CheckNoLeft(intersections.GetTile(pos));
                bool noRight = CheckNoRight(intersections.GetTile(pos));
                // Check left, down, right
                float dD, rD, lD;
                if (!walls.GetTile(pos + Vector3Int.down)) {
                    dD = Vector2.Distance(pos + Vector3.down, target);
                }
                else {
                    dD = 9999f;
                }

                if (!noRight && !walls.GetTile(pos + Vector3Int.right)) {
                    rD = Vector2.Distance(pos + Vector3.right, target);
                }
                else {
                    rD = 9999f;
                }

                if (!noLeft && !walls.GetTile(pos + Vector3Int.left)) {
                    lD = Vector2.Distance(pos + Vector3.left, target);
                }
                else {
                    lD = 9999f;
                }

                if (lD <= rD && lD <= dD) {
                    dir = Direction.left;
                }
                else if (dD <= lD && dD <= rD) {
                    dir = Direction.down;
                }
                else { // go right
                    dir = Direction.right;
                }
            }
            // Check if we are going to be in front of a wall
            else if (walls.GetTile(pos + Vector3Int.down)) {
                // Go either left or right, can't go up
                if (!walls.GetTile(pos + Vector3Int.left)) {
                    dir = Direction.left;
                }
                else if (!walls.GetTile(pos + Vector3Int.right)) {
                    dir = Direction.right;
                }
            }
            // No matter what, we are moving down one tile next
            transform.DOMove(transform.position + Vector3.down, speed).OnComplete(onMoveCompleted).SetEase(Ease.Linear);
        }
        #endregion
    }

	Vector3Int GetMapCoord(Vector3 pos) {
		// (int) takes the floor of the float
		return new Vector3Int((int)pos.x, (int)pos.y, (int)pos.z);
    }

    bool CheckNoLeft(TileBase tile) {
        return tile.name == "IntersectionNoLeft";        
    }

    bool CheckNoRight(TileBase tile) {
        return tile.name == "IntersectionNoRight";
    }
}
